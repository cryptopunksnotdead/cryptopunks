
Welcome to the Home of the Ordinal Punks Text-To-Image Generation Algorithm ([& Spritesheet](https://github.com/cryptopunksnotdead/punks.spritesheet))



# cryptopunks libraries, tools & scripts


- [**cryptopunks**](cryptopunks) - generate your own 24×24 pixel punk images (off-chain) from the True Official Genuine Matt & John's® Punks sha256-verified original 10 000 unique character collection; incl. 2x/4x/8x zoom for bigger sizes
- [**cryptopunks-gui**](cryptopunks-gui) - (crypto) pixel punks graphical user interface (gui) for simplified image generation
- [**synthpunks**](synthpunks) - (free unlimited) 24×24 pixel punks for everyone - yes, you can - generate / claim your own synth(etic) punks ("off-blockchain") for your own (ethereum) account 40-hexdigit addresses e.g. 0x054f3b6eadc9631ccd60246054fdb0fcfe99b322; incl. 2x/4x/8x zoom for bigger sizes



More / Experimental

- [cryptopunks-graphql](cryptopunks-graphql) - (lite) cryptopunks (http json) graphql api / client






## How-Tos, Talks  & Collection Notes


[**Inside the Billion Dollar $$$ (Crypto) Punk Pixel Heads**](insidepunks)

[**Awesome 100 Ordinal Punks (Anno 2023) Notes**](awesome-ordinalpunks) - 24×24 Pixel Art on the (Bitcoin) Blockchain



[**100 Ordinal Punks - The Free White Label Quick Starter Edition**](ordinalpunks) - Let's (re)create from zero / scratch a pixel-perfect
copy of the Ordinal Punks collection (Anno 2023) using text prompts ("attributes").





## Code / Build / Generate Your Own (Pixel) Punks In Your Own (Programming) Language - Yes, You Can!


### Python


[pixegami/pixel-punk-avatars](https://github.com/pixegami/pixel-punk-avatars) - a (getting started) script for generating (pixel) punk avatars in python



### Go (Lang)

[cryptopunksnotdead/lets-go-programming-cryptopunks](https://github.com/cryptopunksnotdead/lets-go-programming-cryptopunks) - Let's Go! Programming (Crypto) Pixel Punk Profile Pictures & (Generative) Art with Go - Step-by-Step Book / Guide Inside Unique 24×24 Pixel Art on the Blockchain...

[learnpixelart/pixelart.go](https://github.com/learnpixelart/pixelart.go) - Let's Go! Pixel Art



### JavaScript

[cryptopunksnotdead/punks.js](https://github.com/cryptopunksnotdead/punks.js) - draw punk (pixel) art images using any design (in ascii text) in any colors; incl. 2x/4x/8x zoom for bigger sizes and more

[learnpixelart/pixelart.js](https://github.com/learnpixelart/pixelart.js) - Programming Pixel Art in JavaScript




### Solidity


[stephancill/synthetic-punks](https://github.com/stephancill/synthetic-punks) - a unique, fully on-chain (pixel) punk associated with each ethereum address

[0xTycoon/punk-blocks](https://github.com/0xTycoon/punk-blocks) - (pixel) punk attributes on-chain



Anything missing? Yes, you can. Add your source code repo (or step-by-step programming guide).




## License

The scripts (& notes) are dedicated to the public domain.
Use it as you please with no restrictions whatsoever.



## Questions? Comments?

Post them on the [D.I.Y. Punk (Pixel) Art reddit](https://old.reddit.com/r/DIYPunkArt). Thanks.



